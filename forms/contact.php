<?php
//echo "SteFFFF";
ini_set('display_errors', 1);
$curl = curl_init();

$email = $_POST['email'];
$sujet = $_POST['sujet'] ?? "";
$message = $_POST['message'];
$name = $_POST['nom']. " ". $_POST['prenom'];

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://coisa.io/automation/email_to_coisa.php",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "sujet=$sujet&message=$message&msg=$message&email=$email&name=$name",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;